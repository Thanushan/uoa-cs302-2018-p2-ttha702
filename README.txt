INSTRUCTIONS
RUN cherrypy_example.py


Application runs following README instructions on Linux -DONE(
User can log in -DONE (pqua613, dtri542)
User can see who is currently online -DONE (pqua613, dtri542)
User can see and edit their profile page -DONE 
User can send, receive, and view messages and files with someone online - DONE (sdhu434,ccho416)

Automatically refreshing page (or refreshing content) and/or notifications -DONE 
Good use of database(s) -DONE
Retrieve profiles for other users, provide profiles to other users -DONE (sdhu434,ccho416)	  
Unicode support (including emojis) -DONE (sdhu434,ccho416)
	 
(Good) Page Templating --DONE
Modular and Pythonic code, including commenting and documentation -DONE

Nice User Interface - DONE