#!/usr/bin/python
""" cherrypy_example.py

    COMPSYS302 - Software Design
    Author: Andrew Chen (andrew.chen@auckland.ac.nz)
    Last Edited: 19/02/2018

    This program uses the CherryPy web server (from www.cherrypy.org).
"""
# Requires:  CherryPy 3.2.2  (www.cherrypy.org)
#            Python  (We use 2.7)

# The address we listen for connections on
listen_ip = '0.0.0.0'
listen_port = 10009

import cherrypy
import urllib
import urllib2
import hashlib
import socket
import os
import json
import sqlite3
import base64
import time
from jinja2 import Environment, FileSystemLoader

CURR_DIR = os.path.dirname(os.path.abspath(__file__))
env = Environment(loader=FileSystemLoader(CURR_DIR), trim_blocks=True)

# print "THIS SHOULD BE IP"
ip = socket.gethostbyname(socket.gethostname())
# print socket.gethostbyname(socket.gethostname())



class MainApp(object):
    # CherryPy Configuration
    _cp_config = {'tools.encode.on': True,
                  'tools.encode.encoding': 'utf-8',
                  'tools.sessions.on': 'True',
                  }

    # If they try somewhere we don't know, catch it here and send them to the right place.
    @cherrypy.expose
    def default(self, *args, **kwargs):
        """The default page, given when we don't recognise where the request is for."""
        Page = "I don't know where you're trying to go, so have a 404 Error."
        cherrypy.response.status = 404
        return Page

    # PAGES (which return HTML that can be viewed in browser)
    @cherrypy.expose
    def index(self):
        try:
            checkOnlineUsers = "http://cs302.pythonanywhere.com/getList?" + \
                               "username=" + cherrypy.session['username'] + "&password=" + cherrypy.session['password'] + \
                               "&enc=0&json=1"
            cherrypy.session['getList'] = checkOnlineUsers

            input_dict = self.onlineUsers()

            template = env.get_template('sidebar.html')
            return template.render(username=cherrypy.session['username'],
                                   dict=input_dict)

        except KeyError:  # There is no username
            raise cherrypy.HTTPRedirect('/login')

    # opens chat box with selected user
    @cherrypy.expose
    def message(self, profile_username):
        input_dict = self.onlineUsers()
        # Gets list of sent messages from database
        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("SELECT * FROM Messaging WHERE sender='ttha702' AND destination=?", (profile_username,))
        sendingDetails = c.fetchall()
        db.commit()
        # Gets list of received messages from database
        c.execute("SELECT * FROM Messaging WHERE destination='ttha702' AND sender=?", (profile_username,))
        receivingDetails = c.fetchall()
        db.commit()
        db.close()

        template = env.get_template('messenger.html')
        return template.render(username=cherrypy.session['username'], profile_username=profile_username,
                               dict=input_dict, user=sendingDetails, inbox=receivingDetails)

    # Redirects to signin page
    @cherrypy.expose
    def login(self):
        template = env.get_template('Signin.html')
        return template.render()

    # Views profile of selected user
    @cherrypy.expose
    def profile(self, profile_username=None):
        # Fetch ip address and port of selected user
        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("SELECT * FROM userlist WHERE username=?", (profile_username,))
        userDetails = c.fetchone()
        db.commit()
        db.close()
        input_dict = self.onlineUsers()
        profileURL = 'http://' + userDetails[2] + ':' + userDetails[3] + '/getProfile'
        profile_dict = self.retrieveProfile(profileURL, profile_username)
        template = env.get_template('profile.html')
        return template.render(username=cherrypy.session['username'],
                               dict=input_dict, profile=profile_dict)

    # Requests latest profile details from selected user
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def retrieveProfile(self, profileURL, profile_username):
        output_dict = {"sender": "ttha702", "profile_username": profile_username}
        data = json.dumps(output_dict)
        req = urllib2.Request(profileURL, data, {'Content-Type': 'application/json'})
        response = urllib2.urlopen(req).read()
        return json.loads(response)

    # Sends profile details to whoever is requesting as a json dictionary
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def getProfile(self):
        # Gets own profile details from database
        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("SELECT * FROM profileInfo ")
        profileDetails = c.fetchone()
        db.commit()
        db.close()

        myProfile = {"fullname": profileDetails[0], "description": profileDetails[1],
                     "lastUpdated": profileDetails[5],
                     "position": profileDetails[2], "location": profileDetails[3],
                     "picture": profileDetails[4]}
        data = json.dumps(myProfile)
        return data

    # Redirects to Edit Profile Page
    @cherrypy.expose
    def editProfile(self):
        template = env.get_template('editProfile.html')
        return template.render(username=cherrypy.session['username'])

    # Updates database with users new profile info that was entered
    @cherrypy.expose
    def updateProfile(self, fullName=None, description=None,position=None, picture=None,
                      location=None):
        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("""UPDATE profileInfo SET fullName=?, position=?, location=?,
                    description=?, picture=?, lastUpdated=?""",
                  (fullName, position, location, description, picture, time.time()))
        db.commit()
        db.close()
        # Redirects to profile page to show user their updated profile
        return self.profile('ttha702')

    @cherrypy.expose
    def sum(self, a=0, b=0):  # All inputs are strings by default
        output = int(a) + int(b)
        return str(output)

    # LOGGING IN
    @cherrypy.expose
    def signin(self, username=None, password=None):
        """Check their name and password and send them either to the main page, or back to the main login screen."""
        error = self.authoriseUserLogin(username, password)
        if error == 0:
            cherrypy.session['username'] = username
            hash_pass = hashlib.sha256(password + username).hexdigest()
            cherrypy.session['password'] = hash_pass
            raise cherrypy.HTTPRedirect('/')
        else:
            raise cherrypy.HTTPRedirect('/login')

    # LOGGING OUT
    @cherrypy.expose
    def signout(self, username=None, password=None):
        """Logs the current user out, expires their session"""
        error = self.authoriseUserLogout(username, password)
        if error == 1:
            raise cherrypy.HTTPRedirect('/logout')
        else:
            cherrypy.lib.sessions.expire()
            raise cherrypy.HTTPRedirect('/')
    # Logs into database if username and password correct
    @cherrypy.expose
    def authoriseUserLogin(self, username=None, password=None):
        pythonAnywhere = ('http://cs302.pythonanywhere.com/report?username=' +
                          username.lower() + '&password=' + hashlib.sha256(password + username).hexdigest() +
                          '&ip='+ ip + '&port='+
                          str(listen_port) +'&location=0&enc=0')
        if urllib.urlopen(pythonAnywhere).read()[0] == '0':
            return 0
        else:
            return 1

    # Reorts to database that user is logging out
    @cherrypy.expose
    def authoriseUserLogout(self, username, password):
        pythonAnywhere = ('http://cs302.pythonanywhere.com/logoff?username=' +
                          cherrypy.session['username'] + '&password=' + cherrypy.session['password'] + '&enc=0')
        if urllib.urlopen(pythonAnywhere).read()[0] == '0':
            return 0
        else:
            return 1

    # Retrieves udated list of who is currently active from main server
    @cherrypy.expose
    def onlineUsers(self):
        checkOnlineUsers = "http://cs302.pythonanywhere.com/getList?" + \
                           "username=" + cherrypy.session['username'] + "&password=" + cherrypy.session[
                               'password'] + \
                           "&enc=0&json=1"
        input_data = urllib.urlopen(checkOnlineUsers).read()
        input_dict = json.loads(input_data)
        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("DELETE FROM userList")
        db.commit()

        for i in input_dict:
            c.execute("INSERT INTO userList VALUES (:username,:location,:ip,:port,:lastLogin)",
                      {'username': input_dict[i]['username'], 'location': input_dict[i]['location'],
                       'ip': input_dict[i]['ip'], 'port': input_dict[i]['port'],
                       'lastLogin': input_dict[i]['lastLogin']})
        db.commit()
        db.close()
        return input_dict

    # Inserts message into database whenever someone sends a message to user
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveMessage(self):
        input_message = cherrypy.request.json
        print "MESSAGE IS:"
        print type(input_message)
        print input_message

        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("INSERT INTO Messaging VALUES(:message,:stamp,:destination,:sender)",
                  {'message': input_message['message'], 'stamp': input_message['stamp'],
                   'destination': input_message['destination'], 'sender': input_message['sender']})
        db.commit()
        db.close()
        return "0"

    # Sends Message to selected user
    @cherrypy.expose
    def sendMessage(self, input_message=None, input_username=None):
        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("SELECT * FROM userlist WHERE username=?", (input_username,))
        print "USER DETAILS ARE"
        userDetails = c.fetchone()
        print userDetails
        db.commit()
        db.close()
        messageURL = 'http://' + userDetails[2] + ':' + userDetails[3] + '/receiveMessage'

        #pingURL = 'http://' + userDetails[2] + ':' + userDetails[3] + '/ping?sender=ttha702'
        #if urllib.urlopen(pingURL).read()[0] == '0':
        self.retrieveMessage(input_username, input_message, messageURL)
        return self.message(input_username)
        #else:
        #    print "PING ERROR"
        #    raise cherrypy.HTTPRedirect('/')

    # Requests ReceiveMessage API of selected user you are sending message to
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def retrieveMessage(self, input_username=None, input_message=None, messageURL=None):
        output_dict = {"sender": "ttha702", "message": input_message, "destination": input_username,
                       "stamp": time.time()}
        data = json.dumps(output_dict)
        req = urllib2.Request(messageURL, data, {'Content-Type': 'application/json'})
        response = urllib2.urlopen(req)
        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("INSERT INTO Messaging VALUES(:message,:stamp,:destination,:sender)",
                  {'message': input_message, 'stamp': time.time(),
                   'destination': input_username, 'sender': 'ttha702'})
        db.commit()
        db.close()
        return "0"


    # Updates database whenever a file is received from someone
    @cherrypy.expose
    @cherrypy.tools.json_in()
    def receiveFile(self):
        input_file = cherrypy.request.json
        print "FILE IS"
        print input_file
        input_file['file'] = base64.b64decode(input_file['file'])
        with open(input_file['filename'], 'wb') as f:
            f.write(input_file['file'])

        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("INSERT INTO Messaging VALUES(:message,:stamp,:destination,:sender)",
                  {'message': input_file['filename'], 'stamp': input_file['stamp'],
                   'destination': input_file['destination'], 'sender': input_file['sender']})
        db.commit()
        db.close()
        return "0"

    # Updates database whenever file is sent to someone
    @cherrypy.expose
    def sendFile(self, myFile=None, input_username=None):
        fileName = str(myFile.filename)
        fileType = str(myFile.content_type.value)
        bruceLee = base64.b64encode(myFile.file.read())
        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("SELECT * FROM userlist WHERE username=?", (input_username,))
        userDetails = c.fetchone()
        print userDetails[0]
        db.commit()
        db.close()
        messageURL = 'http://' + userDetails[2] + ':' + userDetails[3] + '/receiveFile'
        self.retrieveFile(messageURL, fileName, fileType, bruceLee, input_username)
        print "File Sent"
        return self.message(input_username)

    # Sends file to selected user by requesting their reveiveFile API
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def retrieveFile(self, messageURL, fileName, fileType, bruceLee, input_username):
        output_dict = {"sender": "ttha702", "destination": input_username,
                       "file": bruceLee, "filename": fileName, "content_type": fileType,
                       "stamp": time.time()}
        data = json.dumps(output_dict)
        req = urllib2.Request(messageURL, data, {'Content-Type': 'application/json'})
        response = urllib2.urlopen(req)
        db = sqlite3.connect('userList.db')
        c = db.cursor()
        c.execute("INSERT INTO Messaging VALUES(:message,:stamp,:destination,:sender)",
                  {'message': output_dict['filename'], 'stamp': output_dict['stamp'],
                   'destination': output_dict['destination'], 'sender': output_dict['sender']})
        db.commit()
        db.close()
        return "0"

    # PING
    @cherrypy.expose
    def ping(self, sender):
        print "PING USED"
        print sender
        return "0"


def runMainApp():
    conf = {'/': {
        'tools.staticdir.on': True,
        'tools.staticdir.root': os.path.abspath(os.getcwd()),
        'tools.staticdir.dir': ''
    }
    }

    # Create an instance of MainApp and tell Cherrypy to send all requests under / to it. (ie all of them)
    cherrypy.tree.mount(MainApp(), "/", conf)

    # Tell Cherrypy to listen for connections on the configured address and port.
    cherrypy.config.update({'server.socket_host': listen_ip,
                            'server.socket_port': listen_port,
                            'engine.autoreload.on': True,
                            })

    print "========================="
    print "University of Auckland"
    print "COMPSYS302 - Software Design Application"
    print "========================================"

    # Start the web server
    cherrypy.engine.start()

    # And stop doing anything else. Let the web server take over.
    cherrypy.engine.block()


# Run the function to start everything
runMainApp()

